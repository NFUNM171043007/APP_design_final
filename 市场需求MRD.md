# 市场需求MRD

## 一、市场分析
###  市场背景

随着政策推动、经济社会环境影响，服饰穿戴消费市场稳定增长，根据艾媒咨询的数据显示，消费者除了在线下门店进行试穿购买，线上电商平台的购买也逐步成为了近年来的发展趋势，其中女性用户的比例高达82.5%，在穿戴市场占主导地位。在新时代背景下，特卖电商平台不仅更能吸引消费者，其获得的认可度也更高。但相比较于线下可以进行试穿的服务，线上平台仍会存在不少用户发现所购买的商品拿到手后不合适自己的这一痛点问题。针对这一问题，有一些商家已经推出了AR试用服务，如天猫中YSL店铺就可以在线试用美妆，以便于用户选择更为合适自己的商品。

## 二、用户调研
### 1.调查人群：85后到00后青年群体

### 2.调研方式：[调查问卷链接](https://www.wjx.cn/m/51944083.aspx)

### 3.调研结果及分析：
本次调查主要参与人群95后、00后群体分别占总调查人群的58.21%、23.88%，其中88.06%的人有过选择衣服的困难，说明青年群体普遍有选购衣服的困难。
![统计1.jpg](https://images.gitee.com/uploads/images/2019/1211/213053_3c3b9ba4_1648216.jpeg)
有32.84%和41.79%的人会经常和有时听取别人的意见选购衣服，不会的人群仅仅占5.97%，说明大部分人有寻求购买衣服建议的需求。在获得选择衣服建议的渠道上，大部分人会直接基于电商平台的推荐、社交平台的推荐或线下实体店推荐，分别占比71.64%、67.16%、64.18%，还有44.78%的人会听取亲朋好友的意见选购衣服。
![统计2](https://images.gitee.com/uploads/images/2019/1211/213054_9f9d6396_1648216.png)
超过半数的被调查对象有使用个性化穿搭推荐APP的意愿，在希望穿搭助手APP有什么功能的问题上，83.58%的被调查人群希望APP有不同风格的衣服推荐。此外每日穿搭推荐占比64.18%，换季推荐占比62.69%，好物推荐占比59.7%
![统计3.jpg](https://images.gitee.com/uploads/images/2019/1211/213053_99089b26_1648216.jpeg)
通过此次问卷调查可知大部分85后到00后的青年群体存在选择衣服的困难，有寻求穿搭建议的需求，也有意愿使用个性穿搭推荐的APP。
## 竞品分析

### 小红书：笔记分享平台

[小红书](https://www.xiaohongshu.com/)是一个生活方式分享平台，小红书社区里内容包含美妆、个护、运动、旅游、家居、酒店、餐馆的信息分享，触及消费经验和生活方式的众多方面。小红书商城采用口碑营销、选品数据结构化等模式，提供优质商品。同时设有 **跨境电商** 部分，通过用户分享自己的产品使用体验来吸引用户，同时邀请各大明星、网红入驻，为用户提供更多好物推荐内容，实现 **笔记浏览、商品购买、心得分享** 的一个良性循环。

 **优劣势分析：** 

 **(1)个性化推荐。** 小红书的各项内容都有标签,将购物笔记按地区、品牌、用途等分类让消费者一目了然,并根据用户的评论、点赞及心愿单及时更新个性化商品,提供更精准的产品推荐及选购指南。

 **(2)口碑营销。** 用口碑营销取代电商的比价销售,是小红书的一大竞争优势。

 **(3)定位明确。** 小红书市场目标人群定位以18-30岁的中高层消费女性为主,当中白领人员占多数。

 **(4)产品质量无法保证。** 近几年来，小红书作为跨境电商平台的飞速发展，其内容包括但不限于服装部分，同时由于用户大量涌入，服装内容推荐及商品购买的质量无法得到保障，存在着一定的短板。

### 好搭盒子：虚拟试衣间

好搭盒子是18年5月正式上线的一个服装电商平台，其中较为有特色的服务是 **虚拟试衣服务** ，用户通过输入自己的身材数据（身高体重、围度、局部比例等），也可以上传自己的真实照片，调节肤色、发型等细节来完成对自身形象的模拟设计，在完成虚拟形象的设计后，用户就可以看到产品穿在自己的虚拟形象上的效果。除了展示单独产品效果外，好搭还能提供一系列的 **服装推荐及效果展示** 。

 **优劣势分析：** 

 **(1)平台服务明确。** 平台首先会基于用户的个体体征和大量的用户反馈等结构化数据来推送产品，并提供在线虚拟试衣服务，对于合适且喜欢的产品，用户可以选择在家先试后买，物流费用由平台承担。

 **(2)结构化用户细颗粒画像。** 种子用户的获取，好搭通过建立各大女性社区平台的博主矩阵，让博主为产品说故事的方式快速聚集各类细分人群的需求流量，并引导进App/小程序，以便让用户在使用之前就可以完全了解好搭盒子的玩法。

 **(3)“营造了一种美好的想象”。** “货不对板”是电商购物的一个痛点、虚拟试衣也更具有场景感地呈现了商品，但并不足够具有说服力。
### 目标用户(性别限定：女性)

- **年龄段18-40岁**：经过用户调研得出，此年龄段女性对于外形管理重视程度较高。
- **买买买剁手人群**：有较高的购物欲望，因此入手的服饰较多，搭配难度大。
- **穿搭小白**：想要学习更多的穿搭技巧。
- **学生党**、**年轻白领**：想将有限的资金放在购买适合的服饰上，少走弯路。
- **选择困难症患者**：对于眼花缭乱的服饰无从下手，需要一个贴心服装顾问。

## 产品理念
*为此我们团队开发了一款穿搭类app叫今日衣，这款功能型产品致力于为女性打造全面的穿搭服务，无论您的体型脸蛋如何，app都将为您搭配出您最美的样子。*

**方便精准**：每日穿搭推送-根据定位，获取定位地点天气信息并根据获取的信息生成相应的搭配方案推送用户。

**个性化**：根根据用户选择的个人形体信息和用户照片生成模拟小人自行搭配；

**全面多样**：保障用户各方面需求，提供全面，详细的单品列表供用户选择。
***
## 产品亮点：
### 产品功能
今日衣app产品功能主要分为：**今日穿搭、量身定制、我的衣柜、标签分类**
- 优先级：

**今日穿搭-重要**

**量身定制-重要**

**我的衣柜-重要**

**标签分类-次重要**

 **今日穿搭**
根据用户的定位，系统获取定位地点天气信息并根据获取的信息生成相应的搭配方案推送用户。

**- 亮点**
系统自动获取当地天气温度，无需担心系统推荐的搭配与今日的天气条件不符。
***
**量身定制**
根据用户选择的个人形体信息推荐适合用户的衣款红名单与不适合用户的衣款黑名单；根据用户选择的个人形体信息生成模拟小人自行搭配；

**- 亮点**
**1** 个性化，针对用户个人体表特征进行定制推荐。

**2** 数据系统筛选，精准推荐。
***
**我的衣柜**
对于手上难以搭配的衣服，拍摄一张照片并上传，系统自动识别并获取单品多种搭配方案推送用户。

**- 亮点**
**1** 图像识别的置入，提高app智能化。

**2** 方便快捷，仅需两步操作便可获得搭配方案。
***
**标签分类**
标签列出主题/需求/，例如：显瘦/显高/日系/遮腿粗/休闲等，点击标签便可获得相应搭配方案。

**- 亮点**
直接的标签展现，方便用户获取个人搭配意愿
***